package com.brst.donationapp.controller;


public class Config {
    //    http://beta.brstdev.com/freedrinkz/api/web/
//    public static String APP_URL="http://192.168.1.142/free/yii2-advanced-api/api/web/";
        public static String APP_URL="http://beta.brstdev.com/freedrinkz/api/web/";
    public static String APP_URL1="http://192.168.1.13/freedrinkz/api/web/";
    public static String POST_URL="v1/";
//    192.168.1.13/freedrinkz/api/web
//    public static String IMAGE_POST_URL="http://192.168.1.142/free/yii2-advanced-api/api/web/v1/users/";
public static String IMAGE_POST_URL="http://beta.brstdev.com/freedrinkz/api/web/v1/users/";
    //    http://192.168.1.142/free/yii2-advanced-api/api/web/
    static String GCM_PROJECT_NUMBER = "";
    static String BASE_URL = "";

    static AppMode appMode = AppMode.TEST;
    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }

    static public String getBaseURL1() {

        return APP_URL1+POST_URL;
    }

    static public String getGCMProjectNumber() {

        init(appMode);

        return GCM_PROJECT_NUMBER;
    }

    /**
     * Initialize all the variable in this method
     *
     * @param appMode
     */
    public static void init(AppMode appMode) {

        switch (appMode) {
            case DEV:
                BASE_URL =APP_URL+POST_URL;
//                GCM_PROJECT_NUMBER = "563232976573";
                break;

            case TEST:
                BASE_URL =APP_URL+POST_URL;
//                GCM_PROJECT_NUMBER = "563232976573";
                break;

            case LIVE:
                BASE_URL =APP_URL+POST_URL;
//                GCM_PROJECT_NUMBER = "563232976573";
                break;
        }

    }

    public enum AppMode {
        DEV, TEST, LIVE
    }

}
