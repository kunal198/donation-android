package com.brst.donationapp.controller;




import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiInterface {


  /*  @POST("fcm/send")
    Call<JsonObject> sendNotification(@Body JsonObject object);

    @POST("members/createCustomer")
    Call<JsonObject> createCustomerOnStripe(@Body JsonObject object);*/

//    loginkey : 95nwS2Xh transaction key:        54T63r839fAtATqP

    @GET("api?option=get_video&category=7")
    Observable<List<HashMap>> getData();

    @GET("api/?option=get_category_list")

    Observable<List<HashMap>> getData1();

}
