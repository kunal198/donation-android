package com.brst.donationapp.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.brst.donationapp.R;
import com.google.gson.JsonObject;


import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by brst-pc97 on 3/24/17.
 */
public class Utilites {
//    PhSMzzJfr8kssCUOA6UBZrO6z
//    PntQn8FghsKQy1oFwVG0CFXXu56xO29Rw6r3r3VUyIUR6Iuo0D

    private Context context;

    public static boolean isInternetOn(Context context) {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

            // if connected with internet

//            Toast.makeText(context, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

            Toast.makeText(context, " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


    public static void showLogoutPopUp(final Context mContext) {
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Logout");
            builder.setMessage("Are you sure you want to logout? All your information related to groups join have been lost once you logout.");

            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // positive button logic



                        }
                    });


            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // negative button logic
                        }
                    });
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            // display dialog
            dialog.show();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    ProgressDialog progressDialog;

    /**
     * @param context
     * @return Returns true if there is network connectivity
     */


    public static boolean isInternetConnection(Context context) {
        try {

            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    /**
     * Display Toast Message
     **/
    public static void showToastMessageShort(Context context, String message) {
        Toast.makeText(context.getApplicationContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Display Toast Message
     **/
    public static void showToastMessageLong(Context context, String message) {
        Toast.makeText(context.getApplicationContext(), message,
                Toast.LENGTH_LONG).show();
    }


    public void cancelProgerssDialog() {
        if ((this.progressDialog != null) && (this.progressDialog.isShowing())) {
            this.progressDialog.dismiss();
        }
    }

//    public static void openClass(final DataPojo commonResponse, Context mContext) {
////        if (commonResponse.getUsertype().trim().equals("1")) {
//        CallProgressWheel.showLoadingDialog(mContext, "Loading...");
//
//        Log.i("Resturant_id", commonResponse.getRestuarant_id()+"==CheckInStatus=="+commonResponse.getCheck_in_status());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_USER_ID, commonResponse.getUser_id()+"");
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_AUTH_TOKEN, commonResponse.getToken());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_LOGIN_TYPE, commonResponse.getUsertype());
//
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_FIRST_NAME, commonResponse.getFirstname());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_LAST_NAME, commonResponse.getLastname());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_EMAIL, commonResponse.getEmail());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_PROFILE_PIC_URL, commonResponse.getProfile_pic());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_STATE, commonResponse.getState());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_CITY, commonResponse.getCity());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_BUSINESS_NAME, commonResponse.getResturant_name());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_BAND_NAME, commonResponse.getBand_name());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_LATITUDE, commonResponse.getLatitude());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_LANGITUDE, commonResponse.getLongitude());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_ADDRESS, commonResponse.getAddress());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_ZIPCODE, commonResponse.getZip_code());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_USER_ONLINE_STATUS, commonResponse.getOnline_status());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_RESTUARANT_ID, commonResponse.getRestuarant_id());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_CHECK_IN_STATUS, commonResponse.getCheck_in_status());
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_RESTUARANT_NAME, commonResponse.getResturant_name());
//
//        // save blocked user list
//        Gson gson = new Gson();
//        String blockedUserList = gson.toJson(commonResponse.getBlockUser());
//        Log.d("TAG","blockedUserList = " + blockedUserList);
//        PreferenceHandler.writeString(mContext, PreferenceHandler.PREF_KEY_BLOCKED_USER, blockedUserList);
//
//        PreferenceHandler.writeInteger(mContext, PreferenceHandler.COMING_FROM, 0);
//
//        signupFireBase(mContext,commonResponse);
//
//
//    }

    public static void showErrorDialog(final Activity mContext, String message) {
        try {

            Log.i("message===", message);
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext,
                    R.style.CustomPopUpThemeBlue);
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean isValidEmailEnter(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";


    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public static boolean validatePassword(final String password) {

        if (!password.matches(".*[A-Z].*")) return false;

        if (!password.matches(".*\\d.*")) return false;

        return true;

    }


    public static boolean checkPassWordAndConfirmPassword(String password, String confirmPassword) {
        boolean pstatus = false;
        if (confirmPassword != null && password != null) {
            if (password.equals(confirmPassword)) {
                pstatus = true;
            }
        }
        return pstatus;
    }

//    public static void getProfileData( final Context context){
//
//        try{
//            CallProgressWheel.showLoadingDialog(context, "Loading...");
//            ApiInterface mWebapi = RetrofitClient.getClient().create(ApiInterface.class);
//              JsonObject  object= new JsonObject();
//
//            mWebapi.getUserData(object).enqueue(new retrofit2.Callback<ResponsePojo>() {
//                @Override
//                public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> commonResponse) {
////                    Log.d(LOG_TAG, "Response " + commonResponse.toString());
//                    if(commonResponse.body() != null) {
//                        if (commonResponse.body().getStatus() == 200) {
//
//                            if(commonResponse.body() != null){
//
////                                openClass(commonResponse.body().getData(), context);
//
//                            }
//
//
//                        } else if (commonResponse.body().getStatus() == 400) {
//                            if(commonResponse.body().getError().getError_code().equals("1002")){
//
//                            }else {
//
//                            }
//                        } else {
//                            Utilites.showErrorDialog((Activity) context, commonResponse.body().getMessage());
//                        }
//                    }
//                    CallProgressWheel.dismissLoadingDialog();
//                }
//
//                @Override
//                public void onFailure(Call<ResponsePojo> call, Throwable t) {
//                    try {
//                        if(t.getMessage().contains("Wrong Email and Password")){
//                            Utilites.showToastMessageShort(context, "Wrong Email and Password");
//
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    CallProgressWheel.dismissLoadingDialog();
//                }
//            });
//        }catch (Exception e){
//            e.getMessage();
//        }
//
//    }

//


    /**
     *
     * @param context
     * update user Online offline status.
     */


    /**
     * @param context
     * @return json object of payload
     */
    private static JsonObject payLoad(String onlineStatus, Context context) {
        JsonObject object = null;
        try {
            object = new JsonObject();
            String userId = "", authToken = "";


            object.addProperty("user_id", userId);
            object.addProperty("auth_token", authToken);
            object.addProperty("online_status", onlineStatus);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return object;
    }


    /**
     * @param context
     * @return json object of payload
     */
    private static JsonObject payLoadClearFcmToken(Context context) {
        JsonObject object = null;
        try {
            object = new JsonObject();
            String userId = "", authToken = "";



            object.addProperty("user_id", userId);
            object.addProperty("auth_token", authToken);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return object;
    }


    public static String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
//            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
//            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM, HH:mm a");
//            Date currenTimeZone = (Date) calendar.getTime();
//            return sdf.format(currenTimeZone);

            String date = DateFormat.format("dd-MMM, HH:mm a", calendar).toString();


            return date;
        } catch (Exception e) {
        }
        return "";
    }


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time, Context ctx) {
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }


//    public static String timeAgo(long millis,Context context) {
//        millis = millis;
//        context = context;
//        long diff = new Date().getTime() - (millis*1000);
//
//        Resources r = context.getResources();
//
//        String prefix = r.getString(R.string.time_ago_prefix);
//        String suffix = r.getString(R.string.time_ago_suffix);
//
//        double seconds = Math.abs(diff) / 1000;
//        double minutes = seconds / 60;
//        double hours = minutes / 60;
//        double days = hours / 24;
//        double years = days / 365;
//
//        String words;
//
//        if (seconds < 45) {
//            words = r.getString(R.string.time_ago_seconds, Math.round(seconds));
//        } else if (seconds < 90) {
//            words = r.getString(R.string.time_ago_minute, 1);
//        } else if (minutes < 45) {
//            words = r.getString(R.string.time_ago_minutes, Math.round(minutes));
//        } else if (minutes < 90) {
//            words = r.getString(R.string.time_ago_hour, 1);
//        } else if (hours < 24) {
//            words = r.getString(R.string.time_ago_hours, Math.round(hours));
//        } else if (hours < 42) {
//            words = r.getString(R.string.time_ago_day, 1);
//        } else if (days < 30) {
//            words = r.getString(R.string.time_ago_days, Math.round(days));
//        } else if (days < 45) {
//            words = r.getString(R.string.time_ago_month, 1);
//        } else if (days < 365) {
//            words = r.getString(R.string.time_ago_months, Math.round(days / 30));
//        } else if (years < 1.5) {
//            words = r.getString(R.string.time_ago_year, 1);
//        } else {
//            words = r.getString(R.string.time_ago_years, Math.round(years));
//        }
//
//        StringBuilder sb = new StringBuilder();
//
//        if (prefix != null && prefix.length() > 0) {
//            sb.append(prefix).append(" ");
//        }
//
//        sb.append(words);
//
//        if (suffix != null && suffix.length() > 0) {
//            sb.append(" ").append(suffix);
//        }
//
//        return sb.toString().trim();
//    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static String loadJSONFromAsset(Context context, String filename) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }





    /**
     * Showing google speech input dialog
     */



    public static double distance(double lat1, double lon1, double lat2, double lon2) {

        try {
//            Location startPoint = new Location("locationA");
//            startPoint.setLatitude(lat1);
//            startPoint.setLongitude(lon1);
//
//            Location endPoint = new Location("locationA");
////            endPoint.setLatitude(30.7082);
////            endPoint.setLongitude(76.6917);
//
//            endPoint.setLatitude(lat2);
//            endPoint.setLongitude(lon2);
//
//            double distance = Math.round(startPoint.distanceTo(endPoint));
//            Log.i("distance", distance+"");
//            return distance/1000;


            double pk = (Double) (180.f/ Math.PI);

            double a1 = lat1 / pk;
            double a2 = lon1 / pk;
            double b1 = lat2 / pk;
            double b2 = lon2 / pk;

            double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
            double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
            double t3 = Math.sin(a1) * Math.sin(b1);
            double tt = Math.acos(t1 + t2 + t3);
            Log.i("distance", tt+"");
            Log.i("distance1", 6366000 * tt+"");
            return 6366000 * tt;
//            return  tt;

        }catch (Exception e){
            e.printStackTrace();
        }

        return 0.0;

    }

    public static String getDate(String OurDate)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(OurDate);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(value);

            //Log.d("OurDate", OurDate);
        }
        catch (Exception e)
        {
            OurDate = "00-00-0000 00:00";
        }
        return OurDate;
    }


    public static boolean validationInput(EditText mEdbox){

        try {
            if(mEdbox.getText().toString().trim().length() == 0){

                return false;
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
}
