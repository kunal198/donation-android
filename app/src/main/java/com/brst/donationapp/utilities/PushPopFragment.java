package com.brst.donationapp.utilities;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.brst.donationapp.R;

/**
 * Created by brst-pc89 on 12/29/17.
 */

public class PushPopFragment {

    static  PushPopFragment instance=new PushPopFragment();

    public static PushPopFragment getInstance()
    {
        return instance;
    }

    public void pushFragment(AppCompatActivity activity, Fragment fragment)

    {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    public static void popBackStack(AppCompatActivity activity) {

        FragmentManager manager =activity.getSupportFragmentManager();
        manager.popBackStack();
    }
}
