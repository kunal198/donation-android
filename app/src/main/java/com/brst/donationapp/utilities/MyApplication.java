package com.brst.donationapp.utilities;

import android.app.Application;
import android.content.Context;

/**
 * Created by brst-pc97 on 12/28/17.
 */

public class MyApplication extends Application {

  public  static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this.getApplicationContext();
    }
}
