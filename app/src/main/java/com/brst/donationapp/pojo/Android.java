package com.brst.donationapp.pojo;

/**
 * Created by brst-pc97 on 1/2/18.
 */

public class Android {
    private String ver;
    private String name;
    private String api;

    public String getVer() {
        return ver;
    }

    public String getName() {
        return name;
    }

    public String getApi() {
        return api;
    }
}
