package com.brst.donationapp.pojo.AllCategory;

/**
 * Created by brst-pc97 on 1/4/18.
 */

public class DataModel {
    public String text;
    public int drawable;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }
}
