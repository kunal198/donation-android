package com.brst.donationapp.ui.activities.forgotscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.renderscript.Element;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;


import com.brst.donationapp.R;
import com.brst.donationapp.ui.activities.changescreen.ChangeActivity;
import com.brst.donationapp.utilities.Error;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class ForgotActivity extends AppCompatActivity implements View.OnClickListener, ForgotInterface.forgotViewInterface {

    static final String KEY_ITEM = "item";
    static final String KEY_ID = "id";
    static final String KEY_NAME = "name";
    static final String KEY_COST = "cost";
    static final String KEY_DESC = "description";

    Toolbar toolBar;

    TextView textViewForgot;

    TextInputEditText textInputEmailForgot;

    ForgotPresenter forgotPresenter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot);

        setUpViews();
    }

    private void setUpViews() {

        context = this;

        toolBar = findViewById(R.id.tooBar);
        textViewForgot = findViewById(R.id.textViewForgot);
        textInputEmailForgot = findViewById(R.id.textInputEmailForgot);

        textInputEmailForgot.setSelection(textInputEmailForgot.getText().length());

        forgotPresenter = new ForgotPresenter(this);

        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        textViewForgot.setOnClickListener(this);

       // getXmlFromUrl();

    }

    private void getXmlFromUrl() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {

            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("https://api.androidhive.info/pizza/?format=xml");
            HttpResponse httpResponse = defaultHttpClient.execute(httpPost);

            String xml = EntityUtils.toString(httpResponse.getEntity());

            Document document=getDomElement(xml);

            NodeList nl = document.getElementsByTagName(KEY_ITEM);



            for (int i=0;i<nl.getLength();i++)
            {
                Element e = (Element) nl.item(i);


            }
        } catch (IOException e) {
        }
    }

    @Override
    public void onClick(View v) {

        forgotPresenter.validateEmail(context, textInputEmailForgot.getText().toString());

    }

    @Override
    public void checkEmail(String message) {

        Error.getInstance().showError(context, findViewById(R.id.textViewForgot), message);

    }

    @Override
    public void navigate() {

        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public Document getDomElement(String xml){
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }
}
