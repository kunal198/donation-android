package com.brst.donationapp.ui.activities.changescreen;

import android.content.Context;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public interface ChangeInterface {

    interface changeViewInterface {

        void checkField(String message);

        void transition();
    }

    interface changeModelInterface {

        public void validateField(Context context, String oldPass, String newPass, String confirmPass, Listener listener);

        interface Listener {

            void onCheckFiled(String message);

            void onTransition();
        }
    }
}
