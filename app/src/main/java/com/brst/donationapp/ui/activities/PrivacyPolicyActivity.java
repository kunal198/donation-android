package com.brst.donationapp.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.brst.donationapp.R;


public class PrivacyPolicyActivity extends AppCompatActivity {

    Toolbar toolBarPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        setUpViews();
    }

    private void setUpViews() {
        toolBarPolicy=findViewById(R.id.toolBarPolicy);
        setSupportActionBar(toolBarPolicy);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.policy_name);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
