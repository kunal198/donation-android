package com.brst.donationapp.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.fragments.DescriptionFragment;
import com.brst.donationapp.ui.fragments.HomeFragment;


/**
 * Created by brst-pc89 on 12/6/17.
 */


public class CharityDetailAdaptor extends FragmentStatePagerAdapter {

    private Context context;


    int size;

    public CharityDetailAdaptor(Context context, FragmentManager fm, int size) {
        super(fm);
        this.context = context;
        this.size = size;

    }


    @Override
    public Fragment getItem(int position) {

        return new DescriptionFragment();

    }

    // This determines the number of tabs
    @Override
    public int getCount() {

        return size;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

        switch (position) {
            case 0:
                return context.getString(R.string.description_name);
            case 1:
                return context.getString(R.string.contacttab_name);

            case 2:

                return context.getString(R.string.project_name);

            default:
                return null;
        }
    }


}


