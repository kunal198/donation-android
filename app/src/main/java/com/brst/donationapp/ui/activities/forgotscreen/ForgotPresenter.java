package com.brst.donationapp.ui.activities.forgotscreen;

import android.content.Context;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public class ForgotPresenter implements ForgotInterface.forgotModelInterface.Listener{

    ForgotInterface.forgotViewInterface forgotViewInterface;
    ForgotInterface.forgotModelInterface forgotModelInterface;

    public ForgotPresenter(ForgotInterface.forgotViewInterface forgotViewInterface) {

        this.forgotViewInterface=forgotViewInterface;
        this.forgotModelInterface=new ForgotModel();
    }

    public void validateEmail(Context context, String email)
    {
        forgotModelInterface.validateEmail(context,email,this);
    }

    @Override
    public void onCheckEmail(String message) {

        forgotViewInterface.checkEmail(message);
    }

    @Override
    public void onNavigate() {

        forgotViewInterface.navigate();
    }
}
