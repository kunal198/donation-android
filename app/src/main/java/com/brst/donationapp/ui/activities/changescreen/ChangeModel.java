package com.brst.donationapp.ui.activities.changescreen;

import android.content.Context;

import com.brst.donationapp.utilities.Error;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public class ChangeModel implements ChangeInterface.changeModelInterface{


    @Override
    public void validateField(Context context, String oldPass, String newPass, String confirmPass, Listener listener) {

        if (oldPass.isEmpty())
        {
            listener.onCheckFiled("Enter old password.");
        }
        else if (newPass.isEmpty())
        {
            listener.onCheckFiled("Enter new Password.");
        }
        else if (confirmPass.isEmpty())
        {
            listener.onCheckFiled("Enter confirm Password.");
        }
        else if (!confirmPass.equals(newPass))
        {
            listener.onCheckFiled("New Password and Confirm Password should match.");
        }

        else if (!Error.getInstance().isNetworkAvailable(context))
        {
            listener.onCheckFiled("Network not Avaiable");
        }
        else
        {
            listener.onTransition();
        }
    }
}
