package com.brst.donationapp.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.brst.donationapp.R;


public class TermsActivity extends AppCompatActivity {

    Toolbar toolBarTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        setUpViews();
    }

    private void setUpViews() {
    toolBarTerms=findViewById(R.id.toolBarTerms);
        setSupportActionBar(toolBarTerms);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.terms_name);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;

                default:
                    return super.onOptionsItemSelected(item);
        }
    }
}
