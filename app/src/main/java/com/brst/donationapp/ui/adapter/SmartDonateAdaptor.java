package com.brst.donationapp.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.activities.CharityDetailActivity;
import com.brst.donationapp.ui.fragments.SmartDonateFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brst-pc89 on 12/30/17.
 */

public class SmartDonateAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<String> dataList;


    public SmartDonateAdaptor(Context context, List<String> dataList) {

        this.context=context;
        this.dataList=dataList;



    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

         View itemView = LayoutInflater.from(context).inflate(R.layout.custom_smart_donate, parent, false);
            return new ItemViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayoutSmart;
        public ViewHolder(View itemView) {
            super(itemView);

            linearLayoutSmart=itemView.findViewById(R.id.linearLayoutSmart);

            linearLayoutSmart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context,CharityDetailActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }

    private class ItemViewHolder extends ViewHolder {


        public ItemViewHolder(View itemView) {
            super(itemView);
        }
    }

public void addData(List<String> dataList)
{
    this.dataList=dataList;
    notifyDataSetChanged();
}
}
