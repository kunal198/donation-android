package com.brst.donationapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.activities.CharityHomeActivity;
import com.brst.donationapp.ui.adapter.CategoryAdapter;
import com.brst.donationapp.utilities.MyApplication;
import com.brst.donationapp.utilities.SpacesItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllCharityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllCharityFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    RecyclerView mRvCategoryListing;
    EditText mEdSearchCategory;
    Button mBtnSearch;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView.LayoutManager mLayoutManager;
    CategoryAdapter mAdapter;

    private OnFragmentInteractionListener mListener;

    public AllCharityFragment() {
        // Required empty public constructor
    }


    public static AllCharityFragment newInstance(String param1, String param2) {
        AllCharityFragment fragment = new AllCharityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_all_charity, container, false);
        setListner(v);

        setHasOptionsMenu(true);

        return v;
    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setAdapter();
            }
        }, 200);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void setListner(View view){

        try {
            if (view != null){
                mRvCategoryListing = (RecyclerView)view.findViewById(R.id.recycler_view_category_search);
                mEdSearchCategory = (EditText)view.findViewById(R.id.ed_search);
                mBtnSearch = (Button)view.findViewById(R.id.btn_search);

                mBtnSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });


            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void  setAdapter(){

        try {
            // The number of Columns
            mLayoutManager = new GridLayoutManager(MyApplication.mContext, 2);
            mRvCategoryListing.setLayoutManager(mLayoutManager);
            int spacingInPixels = 16;
            mRvCategoryListing.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            mAdapter = new CategoryAdapter(MyApplication.mContext);
            mRvCategoryListing.setAdapter(mAdapter);
        }

        catch (Exception e)

        {
            e.printStackTrace();
        }


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inf) {
        inf= getActivity().getMenuInflater();
        inf.inflate(R.menu.all_charity_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()){
           case R.id.action_funnel:

               if(CharityHomeActivity.navigationView != null) {
                   CharityHomeActivity.navigationView.getMenu().getItem(1).setChecked(true);
                   Fragment fragment = new  AllCategorySearchFragment();
                   FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                   fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                           android.R.anim.fade_out);
                   fragmentTransaction.replace(R.id.frame, fragment);
                   fragmentTransaction.commit();
               }
               break;
           case R.id.action_star:
               Toast.makeText(getContext(), "coming soon:star", Toast.LENGTH_SHORT).show();
               break;
       }
        return true;
    }



}
