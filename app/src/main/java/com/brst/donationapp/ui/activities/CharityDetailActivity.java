package com.brst.donationapp.ui.activities;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.adapter.AutoScrollAdaptor;
import com.brst.donationapp.ui.adapter.CharityDetailAdaptor;
import com.brst.donationapp.utilities.AutoScrollViewPager;

public class CharityDetailActivity extends AppCompatActivity implements View.OnClickListener {

    TabLayout tabLayoutDetail;
    ViewPager viewPagerDetail;
    Context context;
    Toolbar toolBarDetail;
    AutoScrollViewPager autoScrollViewPagerDetail;
    AutoScrollAdaptor adaptorViewPager;
    ImageView imageViewLeft, imageViewRight,imageViewFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity_detail);

        context = this;
        setUpViews();
        setData();
        setViewPagerAdaptor();
        setListener();
    }

    private void setListener() {

        imageViewLeft.setOnClickListener(this);
        imageViewRight.setOnClickListener(this);
        imageViewFavorite.setOnClickListener(this);
    }

    private void setData() {

        setSupportActionBar(toolBarDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.detail_name);

        autoScrollViewPagerDetail.startAutoScroll();

        autoScrollViewPagerDetail.setStopScrollWhenTouch(true);

        //  viewpager.setScrollDurationFactor(18);
        autoScrollViewPagerDetail.setAutoScrollDurationFactor(30);

    }

    private void setViewPagerAdaptor() {


        CharityDetailAdaptor viewPagerAdaptor = new CharityDetailAdaptor(context, getSupportFragmentManager(), 3);

        // Set the adapter onto the view pager
        viewPagerDetail.setAdapter(viewPagerAdaptor);
        tabLayoutDetail.setupWithViewPager(viewPagerDetail);


        adaptorViewPager = new AutoScrollAdaptor(getApplicationContext());
        autoScrollViewPagerDetail.setAdapter(adaptorViewPager);
    }

    private void setUpViews() {

        tabLayoutDetail = findViewById(R.id.tabLayoutDetail);
        viewPagerDetail = findViewById(R.id.viewPagerDetail);
        toolBarDetail = findViewById(R.id.toolBarDetail);
        autoScrollViewPagerDetail = findViewById(R.id.autoScrollViewPagerDetail);
        imageViewLeft = findViewById(R.id.imageViewLeft);
        imageViewRight = findViewById(R.id.imageViewRight);
        imageViewFavorite = findViewById(R.id.imageViewFavorite);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_fav, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;

                default:

                    return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.imageViewLeft:

                int currentItemPosition = autoScrollViewPagerDetail.getCurrentItem();
                if (currentItemPosition > 0) {
                    autoScrollViewPagerDetail.setCurrentItem(currentItemPosition - 1);
                }
                break;

            case R.id.imageViewRight:

                int currentItemPositionRight = autoScrollViewPagerDetail.getCurrentItem();
                if (currentItemPositionRight < 8) {
                    autoScrollViewPagerDetail.setCurrentItem(currentItemPositionRight + 1);
                }
                break;

            case R.id.imageViewFavorite:

                if (imageViewFavorite.getDrawable().getLevel()==10)
                {
                    imageViewFavorite.setImageLevel(20);
                }
                else
                {
                    imageViewFavorite.setImageLevel(10);
                }

                break;
        }
    }
}
