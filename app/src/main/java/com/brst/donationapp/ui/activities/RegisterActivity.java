package com.brst.donationapp.ui.activities;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.baseactivities.BaseActivity;
import com.brst.donationapp.utilities.CircleTransform;
import com.brst.donationapp.utilities.ImagePicker;
import com.brst.donationapp.utilities.Utilites;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

public class RegisterActivity extends BaseActivity {

    Toolbar mToolbarRegister;
    TextView  mToolBarTittle;
    EditText mEdFirstName, mEdLastName, mEdPassword, mEdEmail, mEdRoadName, mEdRoadNumber, mEdPhoneNumber, mEdPostalCode, mEdCity;

    AppCompatSpinner et_choose_canton;
    File file;
    ImageView mIvUserProfilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setUpViews();
        setUpData();
    }

    @Override
    public void setUpViews() {
        try {
            mToolbarRegister = (Toolbar) findViewById(R.id.toolbar);
            mToolBarTittle = (TextView)findViewById(R.id.text_toolbar_title);

            mIvUserProfilePic = (ImageView) findViewById(R.id.user_image);

            mEdFirstName =(EditText)findViewById(R.id.et_first_name);
            mEdLastName =(EditText)findViewById(R.id.et_last_name);
            mEdEmail =(EditText)findViewById(R.id.et_last_name);
            mEdPassword =(EditText)findViewById(R.id.et_password);
            mEdRoadName =(EditText)findViewById(R.id.et_road_name);
            mEdRoadNumber =(EditText)findViewById(R.id.et_road_number);
            mEdPhoneNumber =(EditText)findViewById(R.id.et_phone_number);
            mEdPostalCode =(EditText)findViewById(R.id.et_postal);
            mEdCity =(EditText)findViewById(R.id.et_city);
            et_choose_canton = (AppCompatSpinner) findViewById(R.id.et_choose_canton);

        }catch (Exception e){
            e.printStackTrace();
        }
//
    }

    @Override
    public void setUpData() {
        try {
            setSupportActionBar(mToolbarRegister);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
           // getSupportActionBar().setHomeAsUpIndicator(R.mipmap.back_arrow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolBarTittle.setText("Register");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
             try {
                 switch (v.getId()){

                     case R.id.layout_signup:
                         if(validation()){

                             if(Utilites.isInternetOn(this)){

                             }
                         }
                         break;

                     case R.id.f_layout_pick_image:
                           onPickImage();
                         break;

                 }

             }catch (Exception e){
                 e.printStackTrace();
             }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validation(){

         if(!Utilites.validationInput(mEdFirstName)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_first_name));
         return false;
         }else if(!Utilites.validationInput(mEdLastName)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_last_name));
             return false;
         }else if(!Utilites.validationInput(mEdEmail)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_email));
             return false;
         }else if(!Utilites.isValidEmailEnter(mEdEmail.getText().toString().trim())){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_not_valid));
         } else if(!Utilites.validationInput(mEdPassword)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_password));
             return false;
         }else if(!Utilites.validationInput(mEdRoadName)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_road_name));
             return false;
         }else if(!Utilites.validationInput(mEdRoadNumber)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_road_number));
             return false;
         }else if(!Utilites.validationInput(mEdPhoneNumber)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_phone_number));
             return false;
         }else if(!Utilites.validationInput(mEdPostalCode)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_postal_code));
             return false;
         }else if(!Utilites.validationInput(mEdCity)){
             Utilites.showToastMessageShort(this, getResources().getString(R.string.enter_enter_city));
             return false;
         }


        return true;
    }


    //Image Picker
    public void onPickImage() {
        try {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getApplicationContext());
            startActivityForResult(chooseImageIntent, 234);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageURI = null;
        if (data != null) {
            selectedImageURI = data.getData();
        }
        if (selectedImageURI != null) {
            mIvUserProfilePic.setImageURI(null);
//                mIvUserProfilePic.setImageURI(selectedImageURI);
            String path = Utilites.getRealPathFromURI(getApplicationContext(), selectedImageURI);

            file = new File(path);

//            BitmapFactory.Options opts = new BitmapFactory.Options ();
//            opts.inSampleSize = 2;   // for 1/2 the image to be loaded
//            Bitmap thumb = Bitmap.createScaledBitmap (BitmapFactory.decodeFile(path, opts), 70, 70, false);
//            mIvUserProfilePic.setImageBitmap(thumb);

            Glide.with(this).load(path)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mIvUserProfilePic);

        } else {

            Bitmap bitmap = ImagePicker.getImageFromResult(getApplicationContext(), resultCode, data);
            if (bitmap != null) {
                file = ImagePicker.getTempFile(RegisterActivity.this);

                mIvUserProfilePic.setImageBitmap(bitmap);

                Glide.with(this).load(file.getPath())
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new CircleTransform(this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mIvUserProfilePic);
//                uploadFile();
            }
        }
    }


}
