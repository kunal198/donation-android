package com.brst.donationapp.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;

import android.widget.EditText;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.controller.ApiInterface;
import com.brst.donationapp.ui.activities.forgotscreen.ForgotActivity;
import com.brst.donationapp.ui.baseactivities.BaseActivity;
import com.brst.donationapp.utilities.MyApplication;
import com.brst.donationapp.utilities.Utilites;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by brst-pc97 on 12/29/17.
 */

public class LoginActivity extends BaseActivity {

    Toolbar mToolbarLogin;
    TextView mToolBarTittle, text_forgot_password;
    EditText mEdEmail, mEdPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUpViews();
        setUpData();


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://pianoclubhouse.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //[{VideoRuntime=16, VideoTitle=Control, VideoArtist=Tenth Avenue North, SongKey=12, VideoID=1934, InstructorID=6, YouTubePreviewID=NV5hbcj7E2I, PublishDate=2017-12-31, MP4Name=MP4_Control_TAN_23422.mp4}, {VideoRuntime=10, VideoTitle=If We Are Honest, VideoArtist=Francesca Battistelli, SongKey=11, VideoID=1924, InstructorID=6, YouTubePreviewID=8ePJarb92lo, PublishDate=2017-12-30, MP4Name=MP4_IfWeAreHonest_FrB_34533.mp4}, {VideoRuntime=20, VideoTitle=In The Garden, VideoArtist=Emily Ann Roberts, SongKey=6, VideoID=1929, InstructorID=6, YouTubePreviewID=zJnTLd6H1MM, PublishDate=2017-12-26, MP4Name=MP4_InTheGarden_EAR_56453.mp4}, {VideoRuntime=12, VideoTitle=Its Not Over yet, VideoArtist=For King And Country, SongKey=8, VideoID=1925, InstructorID=6, YouTubePreviewID=nhCxucALoUY, PublishDate=2017-12-25, MP4Name=MP4_ItsNotOverYet_FKAC_34533.mp4}, {VideoRuntime=17, VideoTitle=I Want To Live, VideoArtist=Skillet, SongKey=3, VideoID=1926, InstructorID=6, YouTubePreviewID=HJdzNrbvjcM, PublishDate=2017-12-22, MP4Name=MP4_IWantToLive_Sk_23422.mp4}, {VideoRuntime=12, VideoTitle=Feel It, VideoArtist=Toby Mac, SongKey=1, VideoID=1923, InstructorID=6, YouTubePreviewID=ktu3kqMCYGo, PublishDate=2017-12-21, MP4Name=MP4_FeelIt_TM_34533.mp4}, {VideoRuntime=11, VideoTitle=Hallelujah, VideoArtist=Jordan Smith, SongKey=3, VideoID=1907, InstructorID=6, YouTubePreviewID=TrXM3XTunEg, PublishDate=2017-12-15, MP4Name=MP4_Hallelujah_JSm_43453.mp4}, {VideoRuntime=13, VideoTitle=He Knows, VideoArtist=Jeremy Camp, SongKey=5, VideoID=1915, InstructorID=6, YouTubePreviewID=VWTuxCH1BqM, PublishDate=2017-12-12, MP4Name=MP4_HeKnows_JC_3456365.mp4}, {VideoRuntime=22, VideoTitle=All My Hope, VideoArtist=Crowder, SongKey=9, VideoID=1919, InstructorID=6, YouTubePreviewID=gtuWX9sERUs, PublishDate=2017-12-11, MP4Name=MP4_AllMyHope_Cr_23422.mp4}, {VideoRuntime=16, VideoTitle=Higher, VideoArtist=Unspoken, SongKey=1, VideoID=1910, InstructorID=6, YouTubePreviewID=vN0G5hAl_vU, PublishDate=2017-12-08, MP4Name=MP4_Higher_Un_34533.mp4}, {VideoRuntime=23, VideoTitle=He Shall Reign Forevermore, VideoArtist=Chris Tomlin, SongKey=11, VideoID=1917, InstructorID=6, YouTubePreviewID=nk7bsMtErgE, PublishDate=2017-12-04, MP4Name=MP4_HeShallReign_CT_45453.mp4}, {VideoRuntime=15, VideoTitle=O' Lord, VideoArtist=Lauren Daigle, SongKey=8, VideoID=1901, InstructorID=6, YouTubePreviewID=wLJAFeIwIx0, PublishDate=2017-11-29, MP4Name=MP4_OLord_LD_23422.mp4}, {VideoRuntime=16, VideoTitle=Different, VideoArtist=Micah Tyler, SongKey=11, VideoID=1895, InstructorID=6, YouTubePreviewID=1vcVHCO-nl8, PublishDate=2017-11-23, MP4Name=MP4_Different_MT_23422.mp4}, {VideoRuntime=16, VideoTitle=How Sweet The Sound, VideoArtist=Citizen Way, SongKey=10, VideoID=1888, InstructorID=6, YouTubePreviewID=LY-sADJvXTo, PublishDate=2017-11-21, MP4Name=MP4_HowSweetTheSound_CW_345634.mp4}, {VideoRuntime=15, VideoTitle=Hallelujah, VideoArtist=Kelley Mooney, SongKey=8, VideoID=1890, InstructorID=6, YouTubePreviewID=47QOFSxNuWU, PublishDate=2017-11-20, MP4Name=MP4_Halellujah_KM_23452.mp4}, {VideoRuntime=16, VideoTitle=Let Them See You, VideoArtist=JJ Weeks Band, SongKey=1, VideoID=1887, InstructorID=6, YouTubePreviewID=chrJhnVcmR0, PublishDate=2017-11-19, MP4Name=MP4_LetThemSeeYou_JJWB_34532.mp4}, {VideoRuntime=16, VideoTitle=How He Loves, VideoArtist=David Crowder, SongKey=1, VideoID=1889, InstructorID=6, YouTubePreviewID=gAApgR8A0Sc, PublishDate=2017-11-18, MP4Name=MP4_HowHeLoves_DC_345345.mp4}, {VideoRuntime=19, VideoTitle=Multiplied, VideoArtist=Needtobreathe, SongKey=8, VideoID=1885, InstructorID=6, YouTubePreviewID=cEQbmZRUsx0, PublishDate=2017-11-17, MP4Name=MP4_Multiplied_NTB_34563.mp4}, {VideoRuntime=11, VideoTitle=Great Are You Lord, VideoArtist=One Sonic Society, SongKey=10, VideoID=1870, InstructorID=6, YouTubePreviewID=P76liyLoNRU, PublishDate=2017-11-12, MP4Name=MP4_GreatAreYouLord_OSS_234222.mp4}, {VideoRuntime=15, VideoTitle=Broken Things, VideoArtist=Matthew West, SongKey=1, VideoID=1868, InstructorID=6, YouTubePreviewID=xb-J0D2pmBk, PublishDate=2017-11-11, MP4Name=MP4_Broke
        Observable<List<HashMap>> listObservable = retrofit.create(ApiInterface.class)
                .getData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Observable<List<HashMap>> listObservable1 = retrofit.create(ApiInterface.class)
                .getData1().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Observable.just(listObservable).filter(new Predicate<Observable<List<HashMap>>>() {
            @Override
            public boolean test(Observable<List<HashMap>> listObservable) throws Exception {
                return listObservable.contains("InstructorID").equals("6");
            }
        }).subscribe(new Observer<Observable<List<HashMap>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Observable<List<HashMap>> listObservable) {

                Log.d("response", listObservable + "--------bbbgvgv");
            }

            @Override
            public void onError(Throwable e) {

                Log.d("data", e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
        Observable.concat(listObservable, listObservable1).subscribe(new Observer<List<HashMap>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<HashMap> hashMaps) {
                Log.d("response", "sdf");
                Log.d("response", hashMaps + "----ffggffg");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
        Observable.merge(listObservable, listObservable1).subscribe(new Observer<List<HashMap>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override        /*Observable.merge(listObservable, listObservable1).subscribe(new Observer<List<HashMap>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<HashMap> hashMaps) {
                Log.d("response","data");
                Log.d("response", hashMaps.size() + "ffff"+"------"+hashMaps);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/

            public void onNext(List<HashMap> hashMaps) {
                Log.d("response", "data");
                Log.d("response", hashMaps + "");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.d("response", "complete00");
            }
        });


        /*Observable listObservable2 = Observable.zip(listObservable, listObservable1, new BiFunction<List<HashMap>, List<HashMap>, UserAndEvents>() {
            @Override
            public UserAndEvents apply(List<HashMap> hashMaps, List<HashMap> hashMaps2) throws Exception {
                return new UserAndEvents(hashMaps, hashMaps2);
            }
        });

        listObservable2.subscribe(new Observer<UserAndEvents>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UserAndEvents userAndEvents) {
                Log.d("data", userAndEvents + "-----" + userAndEvents.events + "_____hggfvff__" + userAndEvents.user);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/
       /* retrofit.create(ApiInterface.class).getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<HashMap>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                        Log.d("data", d.isDisposed() + "---dsdd");
                    }

                    @Override
                    public void onNext(List<HashMap> hashMaps) {

                        Log.d("data", hashMaps + "----csdfs");
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d("data", e.getMessage() + "----csfvddfs");

                    }

                    @Override
                    public void onComplete() {

                        Log.d("data", "fsdfcsdf----csdfs");
                    }
                });*/

    }

    @Override
    public void setUpViews() {
        try {
            mToolbarLogin = findViewById(R.id.toolbar);
            text_forgot_password = findViewById(R.id.text_forgot_password);
            mToolBarTittle =  findViewById(R.id.text_toolbar_title);
            mEdEmail =  findViewById(R.id.et_username);
            mEdPassword =  findViewById(R.id.et_password);

            mEdEmail.setSelection(mEdEmail.getText().length());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setUpData() {
        try {
            setSupportActionBar(mToolbarLogin);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            mToolBarTittle.setText("Login");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        try {

            switch (v.getId()) {

                case R.id.text_forgot_password:
                    // Utilites.showToastMessageShort(MyApplication.mContext, getResources().getString(R.string.in_progress));

                    Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
                    startActivity(intent);

                    break;
                case R.id.text_sign_up:
//                    Utilites.showToastMessageShort(MyApplication.mContext, getResources().getString(R.string.in_progress));
                    Intent explicitIntentRegister = new Intent(LoginActivity.this, RegisterActivity.class);
                    explicitIntentRegister.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(explicitIntentRegister);

                    break;

                case R.id.layout_signup:
                    if (!Utilites.validationInput(mEdEmail)) {
                        Utilites.showToastMessageShort(MyApplication.mContext, getResources().getString(R.string.fill_user_name));
                    } else if (!Utilites.validationInput(mEdPassword)) {
                        Utilites.showToastMessageShort(MyApplication.mContext, getResources().getString(R.string.fill_password_first));
                    } else if (!Utilites.isValidEmailEnter(mEdEmail.getText().toString().trim())) {
                        Utilites.showToastMessageShort(MyApplication.mContext, getResources().getString(R.string.fill_valid_email));
                    } else {

                        if (Utilites.isInternetOn(MyApplication.mContext)) {
                            Intent explicitIntent = new Intent(LoginActivity.this, CharityHomeActivity.class);
                            explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(explicitIntent);
                            finish();
                        }
                    }

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

class UserAndEvents {
    public UserAndEvents(List<HashMap> user, List<HashMap> events) {
        this.events = events;
        this.user = user;
    }

    public List<HashMap> events;
    public List<HashMap> user;
}
