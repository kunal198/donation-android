package com.brst.donationapp.ui.activities.changescreen;

import android.content.Context;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public class ChangePresenter implements ChangeInterface.changeModelInterface.Listener {

    ChangeInterface.changeViewInterface changeViewInterface;
    ChangeInterface.changeModelInterface changeModelInterface;

    public ChangePresenter(ChangeInterface.changeViewInterface changeViewInterface) {

        this.changeViewInterface=changeViewInterface;
        this.changeModelInterface=new ChangeModel();
    }

    public void validateField(Context context, String oldPass, String newPass, String confirmPass)
    {
        changeModelInterface.validateField(context,oldPass, newPass, confirmPass,this);
    }

    @Override
    public void onCheckFiled(String message) {


        changeViewInterface.checkField(message);
    }

    @Override
    public void onTransition() {

        changeViewInterface.transition();
    }
}
