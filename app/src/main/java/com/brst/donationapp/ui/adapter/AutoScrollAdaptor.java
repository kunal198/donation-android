package com.brst.donationapp.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.utilities.AutoScrollViewPager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by brst-pc89 on 2/9/17.
 */
public class AutoScrollAdaptor extends PagerAdapter {
    Context context;

    List<HashMap> list;
    LayoutInflater inflater;

    //  int images[]={R.mipmap.ic_module_image,R.mipmap.ic_module_image,R.mipmap.ic_module_image};
    public AutoScrollAdaptor(Context context) {
        this.context = context;
        this.list = list;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return 8;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


         View v=inflater.inflate(R.layout.custom_viewpagerlayout,null);



        ((AutoScrollViewPager) container).addView(v, 0);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((AutoScrollViewPager) container).removeView((View) object);
    }

}