package com.brst.donationapp.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.MarshMallowPermissionClass.AbsRuntimeMarshmallowPermission;

public class SplashActivity extends AbsRuntimeMarshmallowPermission {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        if (requestCode == ALL_PERMISSIONS) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent I = new Intent(SplashActivity.this, LoginActivity.class);
                    I.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                    finish();

                    // setUpData();

                }
            }, 2000);


        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestAppPermissions(new String[]{android.Manifest.permission.INTERNET,  android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, R.string.message, ALL_PERMISSIONS);
        }else {
            Intent I = new Intent(SplashActivity.this, CharityHomeActivity.class);
            I.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(I);
            finish();
        }
    }

    private void setupData(){
        try {

        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
