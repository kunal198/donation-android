package com.brst.donationapp.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.ui.activities.CharityDetailActivity;
import com.brst.donationapp.ui.activities.CharityHomeActivity;
import com.brst.donationapp.ui.adapter.SmartDonateAdaptor;
import com.brst.donationapp.utilities.EndlessRecyclerOnScrollListener;
import com.brst.donationapp.utilities.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class SmartDonateFragment extends Fragment implements View.OnClickListener {

    View view;
    SmartDonateAdaptor smartDonateAdaptor;
    RecyclerView recyclerViewDonate;
    List<String> parentDataList = new ArrayList<>();
    List<String> dataList;


    int totalItemCount, lastVisibleItem, visibleThreshold = 1;

    boolean isLoading = false;

    TextView textViewMore;
    SwipeRefreshLayout swipeRefreshLayout;

    public SmartDonateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_smart_donate, container, false);

            setUpViews();

            setAdaptor();

            pullToReferesh();

        }

        return view;
    }

    private void pullToReferesh() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                loadData();
            }
        });
    }

    private void loadData() {

        textViewMore.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                smartDonateAdaptor.addData(addDataToList());

                if (swipeRefreshLayout.isRefreshing()) {

                    swipeRefreshLayout.setRefreshing(false);

                }
            }
        },5000);

    }

    List<String> addDataToList()

    {
        parentDataList = new ArrayList<>();
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");
        parentDataList.add("qwertyu");

        return parentDataList;

    }

    private void setAdaptor() {


        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerViewDonate.setHasFixedSize(true);
        recyclerViewDonate.setLayoutManager(layoutManager);
        smartDonateAdaptor = new SmartDonateAdaptor(getContext(), addDataToList());

        recyclerViewDonate.setAdapter(smartDonateAdaptor);

        recyclerViewDonate.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                if (swipeRefreshLayout!=null && swipeRefreshLayout.isRefreshing()) {

                    swipeRefreshLayout.setRefreshing(false);

                }
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                    textViewMore.setVisibility(View.VISIBLE);

                    isLoading = true;
                } else if (totalItemCount > lastVisibleItem + 1) {
                    textViewMore.setVisibility(View.GONE);
                    isLoading = false;
                }
            }
        });


    }

    private void setUpViews() {

        recyclerViewDonate = view.findViewById(R.id.recyclerViewDonate);
        textViewMore = view.findViewById(R.id.textViewMore);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        textViewMore.setOnClickListener(this);


    }

    public void addData() {

        dataList = new ArrayList<>();
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");
        dataList.add("qwertyu");

        parentDataList.addAll(dataList);

        smartDonateAdaptor.notifyDataSetChanged();

     //   recyclerViewDonate.smoothScrollToPosition(parentDataList.size()-dataList.size());

        isLoading = false;

        textViewMore.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.textViewMore:

                addData();

                break;
        }
    }
}
