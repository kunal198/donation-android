package com.brst.donationapp.ui.activities.forgotscreen;

import android.content.Context;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public interface ForgotInterface {

    interface forgotViewInterface
    {
        void checkEmail(String message);

        void navigate();
    }


    interface forgotModelInterface
    {
         void validateEmail(Context context, String email, Listener listener);

        interface  Listener
        {
            void onCheckEmail(String message);

            void onNavigate();
        }
    }


}
