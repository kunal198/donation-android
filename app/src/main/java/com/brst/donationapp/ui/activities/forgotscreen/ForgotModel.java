package com.brst.donationapp.ui.activities.forgotscreen;

import android.content.Context;

import com.brst.donationapp.R;
import com.brst.donationapp.utilities.Error;

/**
 * Created by brst-pc89 on 12/28/17.
 */

public class ForgotModel implements ForgotInterface.forgotModelInterface{

    @Override
    public void validateEmail(Context context, String email, Listener listener) {

        if (email.isEmpty())
        {
            listener.onCheckEmail(context.getResources().getString(R.string.fill_user_name));
        }
        else if (!Error.getInstance().isValidEmail(email))
        {
            listener.onCheckEmail(context.getResources().getString(R.string.fill_valid_email));
        }
       /* else if (!Error.getInstance().isNetworkAvailable(context))
        {
            listener.onCheckEmail("Network not available.");
        }*/

        else
        {
            listener.onNavigate();
        }
    }


}
