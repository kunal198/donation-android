package com.brst.donationapp.ui.activities.changescreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.brst.donationapp.R;
import com.brst.donationapp.utilities.Error;


public class ChangeActivity extends AppCompatActivity implements ChangeInterface.changeViewInterface, View.OnClickListener {

    Toolbar toolBar;
    TextView textViewFooter;

    ChangePresenter changePresenter;

    TextInputEditText textInputEditTextOld, textInputEditTextNew, textInputConfirm;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change);

        setUpViews();
    }

    private void setUpViews() {

        context=this;

        toolBar = findViewById(R.id.toolBar);
        textViewFooter = findViewById(R.id.textViewFooter);
        textInputEditTextOld = findViewById(R.id.textInputEditTextOld);
        textInputEditTextNew = findViewById(R.id.textInputEditTextNew);
        textInputConfirm = findViewById(R.id.textInputConfirm);

        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.change_name);

        textViewFooter.setText(R.string.change_name);

        changePresenter=new ChangePresenter(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;
            default:

                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void checkField(String message) {

        Error.getInstance().showError(context,findViewById(R.id.textViewFooter),message);
    }

    @Override
    public void transition() {

       finish();

    }



    @Override
    public void onClick(View v) {

            changePresenter.validateField(context,textInputEditTextOld.getText().toString(),textInputEditTextNew.getText().toString(),textInputConfirm.getText().toString());
    }
}
